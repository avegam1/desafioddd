package co.com.sofka.delivery.cuenta;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import co.com.sofka.delivery.cuenta.commands.ActualizarMedioPagoRappiPrime;

public final class ActualizarMedioPagoRappiPrimeUseCase extends UseCase<RequestCommand<ActualizarMedioPagoRappiPrime>, ResponseEvents> {
    @Override
    public void executeUseCase(RequestCommand<ActualizarMedioPagoRappiPrime> actualizarMedioPagoRappiPrimeRequestCommand) {
        var command = actualizarMedioPagoRappiPrimeRequestCommand.getCommand();

        var cuenta = Cuenta.from(
                command.getCuentaId(), repository().getEventsBy(command.getCuentaId().value())
        );
        cuenta.actualizarMedioPagoRappiPrime(command.getMedioPago());

        emit().onResponse(new ResponseEvents(cuenta.getUncommittedChanges()));
    }
}
