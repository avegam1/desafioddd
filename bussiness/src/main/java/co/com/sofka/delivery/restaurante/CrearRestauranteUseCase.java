package co.com.sofka.delivery.restaurante;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import co.com.sofka.delivery.restaurante.commands.CrearRestaurante;

public class CrearRestauranteUseCase extends UseCase<RequestCommand<CrearRestaurante>, ResponseEvents> {
    @Override
    public void executeUseCase(RequestCommand<CrearRestaurante> crearRestauranteRequestCommand) {
        var command = crearRestauranteRequestCommand.getCommand();

        var restaurante = new Restaurante(command.getRestauranteId(), command.getNombre(),
                command.getCostoEnvio());

        emit().onResponse(new ResponseEvents(restaurante.getUncommittedChanges()));
    }
}
