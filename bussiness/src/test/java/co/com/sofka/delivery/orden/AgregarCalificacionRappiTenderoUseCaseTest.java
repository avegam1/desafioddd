package co.com.sofka.delivery.orden;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.repository.DomainEventRepository;
import co.com.sofka.business.support.TriggeredEvent;
import co.com.sofka.delivery.cuenta.values.CuentaId;
import co.com.sofka.delivery.genericvalues.Calificacion;
import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.genericvalues.Telefono;
import co.com.sofka.delivery.orden.events.CalificacionRappiTenderoAgregada;
import co.com.sofka.delivery.orden.events.OrdenEntregada;
import co.com.sofka.delivery.orden.events.OrdenTiendaCreada;
import co.com.sofka.delivery.orden.events.RappiTenderoAsignado;
import co.com.sofka.delivery.orden.values.OrdenId;
import co.com.sofka.delivery.orden.values.Propina;
import co.com.sofka.delivery.orden.values.RappiTenderoId;
import co.com.sofka.delivery.tienda.values.TiendaId;
import co.com.sofka.domain.generic.DomainEvent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
public class AgregarCalificacionRappiTenderoUseCaseTest {
    @InjectMocks
    private AgregarCalificacionRappiTenderoUseCase useCase;

    @Mock
    private DomainEventRepository repository;

    @Test
    void agregarCalificacionRappiTenderoHappyPass(){
        OrdenId ordenId = OrdenId.of("dasd");
        Calificacion calificacion = new Calificacion(4);
        var event = new OrdenEntregada(ordenId, calificacion);

        when(repository.getEventsBy("dasd")).thenReturn(history());
        useCase.addRepository(repository);

        //Act
        var events = UseCaseHandler.getInstance()
                .setIdentifyExecutor(event.getOrdenId().value())
                .syncExecutor(useCase, new TriggeredEvent<>(event))
                .orElseThrow()
                .getDomainEvents();

        //Assert
        var calificacionAgregada = (CalificacionRappiTenderoAgregada)events.get(0);
        Assertions.assertEquals(4 , calificacionAgregada.getCalificacion().value());
    }

    private List<DomainEvent> history(){
        TiendaId tiendaId = TiendaId.of("dd");
        CuentaId cuentaId = CuentaId.of("ddd");
        var event = new OrdenTiendaCreada(
                tiendaId,
                cuentaId
        );
        event.setAggregateRootId("dddd");

        RappiTenderoId rappiTenderoId = RappiTenderoId.of("dasdfas");
        Nombre nombre = new Nombre("Pedro");
        Telefono telefono = new Telefono("123456");
        Propina propina = new Propina(5000D);
        var event2 = new RappiTenderoAsignado(
                rappiTenderoId, nombre, telefono, propina
        );
        return List.of(event, event2);
    }
}
