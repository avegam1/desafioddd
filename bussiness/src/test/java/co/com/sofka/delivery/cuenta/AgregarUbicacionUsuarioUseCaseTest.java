package co.com.sofka.delivery.cuenta;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.repository.DomainEventRepository;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.delivery.cuenta.commands.AgregarUbicacionUsuario;
import co.com.sofka.delivery.cuenta.events.CuentaCreada;
import co.com.sofka.delivery.cuenta.events.UbicacionUsuarioAgregada;
import co.com.sofka.delivery.cuenta.values.CuentaId;
import co.com.sofka.delivery.cuenta.values.UsuarioId;
import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.genericvalues.Telefono;
import co.com.sofka.delivery.genericvalues.Ubicacion;
import co.com.sofka.domain.generic.DomainEvent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
public class AgregarUbicacionUsuarioUseCaseTest {
    @InjectMocks
    private AgregarUbicacionUsuarioUseCase useCase;

    @Mock
    private DomainEventRepository repository;

    @Test
    void agregarUbicacionUsuarioHappyPass(){
        //Arrange
        CuentaId cuentaId = CuentaId.of("ddd");
        Ubicacion ubicacion = new Ubicacion("Bolivar", "Cartagena", "Cll 49 ");
        var command = new AgregarUbicacionUsuario(cuentaId, ubicacion);

        when(repository.getEventsBy("ddd")).thenReturn(history());
        useCase.addRepository(repository);

        //Act
        var events = UseCaseHandler.getInstance()
                .setIdentifyExecutor(command.getCuentaId().value())
                .syncExecutor(useCase, new RequestCommand<>(command))
                .orElseThrow()
                .getDomainEvents();

        //Assert
        var event = (UbicacionUsuarioAgregada)events.get(0);
        Assertions.assertEquals(ubicacion, event.getUbicacion());
    }

    private List<DomainEvent> history(){
        UsuarioId usuarioId = new UsuarioId("dasd");
        Nombre nombre = new Nombre("Enrique");
        Telefono telefono = new Telefono("3208237262");
        Usuario usuario = new Usuario(usuarioId, nombre, telefono);
        var event = new CuentaCreada(
                usuario
        );
        event.setAggregateRootId("dddd");
        return List.of(event);
    }
}
