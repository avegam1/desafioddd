package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.genericvalues.CostoEnvio;
import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.domain.generic.DomainEvent;

public class TiendaCreada extends DomainEvent {
    private final Nombre nombre;
    private final CostoEnvio costoEnvio;

    public TiendaCreada(Nombre nombre, CostoEnvio costoEnvio) {
        super("co.com.sofka.delivery.tienda.events.TiendaCreada");
        this.nombre = nombre;
        this.costoEnvio = costoEnvio;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public CostoEnvio getCostoEnvio() {
        return costoEnvio;
    }
}
