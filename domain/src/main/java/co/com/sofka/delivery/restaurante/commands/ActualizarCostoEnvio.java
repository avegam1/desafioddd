package co.com.sofka.delivery.restaurante.commands;

import co.com.sofka.delivery.genericvalues.CostoEnvio;
import co.com.sofka.delivery.restaurante.values.RestauranteId;
import co.com.sofka.domain.generic.Command;

public class ActualizarCostoEnvio extends Command {
    private final RestauranteId restauranteId;
    private final CostoEnvio costoEnvio;

    public ActualizarCostoEnvio(RestauranteId restauranteId, CostoEnvio costoEnvio){
        this.restauranteId = restauranteId;
        this.costoEnvio = costoEnvio;
    }

    public RestauranteId getRestauranteId() {
        return restauranteId;
    }

    public CostoEnvio getCostoEnvio() {
        return costoEnvio;
    }
}
