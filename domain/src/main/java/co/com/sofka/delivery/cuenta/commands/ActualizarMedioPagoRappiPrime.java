package co.com.sofka.delivery.cuenta.commands;

import co.com.sofka.delivery.cuenta.values.CuentaId;
import co.com.sofka.delivery.genericvalues.MedioPago;
import co.com.sofka.domain.generic.Command;

public class ActualizarMedioPagoRappiPrime extends Command {
    private final CuentaId cuentaId;
    private final MedioPago medioPago;

    public ActualizarMedioPagoRappiPrime(CuentaId cuentaId, MedioPago medioPago){
        this.cuentaId = cuentaId;
        this.medioPago = medioPago;
    }

    public CuentaId getCuentaId() {
        return cuentaId;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }
}
