package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.genericvalues.Calificacion;
import co.com.sofka.domain.generic.DomainEvent;

public class CalificacionActualizada extends DomainEvent {
    private final Calificacion calificacion;

    public CalificacionActualizada(Calificacion calificacion) {
        super("co.com.sofka.delivery.tienda.events.CalificacionActualizada");
        this.calificacion = calificacion;
    }

    public Calificacion getCalificacion() {
        return calificacion;
    }
}
