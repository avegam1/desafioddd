package co.com.sofka.delivery.cuenta.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.domain.generic.DomainEvent;

public class NombreUsuarioActualizado extends DomainEvent {
    private final Nombre nombre;

    public NombreUsuarioActualizado(Nombre nombre) {
        super("co.com.rappi.delivery.cuenta.events.NombreUsuarioActualizado");
        this.nombre = nombre;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
