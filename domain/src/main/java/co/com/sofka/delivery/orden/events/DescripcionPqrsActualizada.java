package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.orden.values.Descripcion;
import co.com.sofka.domain.generic.DomainEvent;

public class DescripcionPqrsActualizada extends DomainEvent {
    private final Descripcion descripcion;

    public DescripcionPqrsActualizada(Descripcion descripcion) {
        super("co.com.sofka.delivery.orden.events.DescripcionPqrsActualizada");
        this.descripcion = descripcion;
    }

    public Descripcion getDescripcion() {
        return descripcion;
    }
}
