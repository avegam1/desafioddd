package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.tienda.values.EmpleadoId;
import co.com.sofka.domain.generic.DomainEvent;

public class NombreEmpleadoActualizado extends DomainEvent {
    private final EmpleadoId empleadoId;
    private final Nombre nombre;

    public NombreEmpleadoActualizado(EmpleadoId empleadoId, Nombre nombre) {
        super("co.com.sofka.delivery.tienda.events.NombreProductoActualizado");
        this.empleadoId = empleadoId;
        this.nombre = nombre;
    }

    public EmpleadoId getEmpleadoId() {
        return empleadoId;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
