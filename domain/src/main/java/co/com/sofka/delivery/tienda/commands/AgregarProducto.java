package co.com.sofka.delivery.tienda.commands;

import co.com.sofka.delivery.genericvalues.Categoria;
import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.genericvalues.Precio;
import co.com.sofka.delivery.tienda.values.TiendaId;
import co.com.sofka.domain.generic.Command;

public class AgregarProducto extends Command {
    private final TiendaId tiendaId;
    private final Categoria categoria;
    private final Precio precio;
    private final Nombre nombre;

    public AgregarProducto(TiendaId tiendaId, Categoria categoria, Precio precio, Nombre nombre){
        this.tiendaId = tiendaId;
        this.categoria = categoria;
        this.precio = precio;
        this.nombre = nombre;
    }

    public TiendaId getTiendaId() {
        return tiendaId;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public Precio getPrecio() {
        return precio;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
