package co.com.sofka.delivery.cuenta.commands;

import co.com.sofka.delivery.cuenta.values.CuentaId;
import co.com.sofka.delivery.cuenta.values.Plan;
import co.com.sofka.delivery.genericvalues.MedioPago;
import co.com.sofka.domain.generic.Command;

public class AgregarRappiPrime extends Command {
    private final CuentaId cuentaId;
    private final Plan plan;
    private final MedioPago medioPago;

    public AgregarRappiPrime(CuentaId cuentaId, Plan plan, MedioPago medioPago){
        this.cuentaId = cuentaId;
        this.plan = plan;
        this.medioPago = medioPago;
    }

    public CuentaId getCuentaId() {
        return cuentaId;
    }

    public Plan getPlan() {
        return plan;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }
}
