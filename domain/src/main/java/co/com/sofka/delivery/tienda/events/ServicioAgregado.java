package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.genericvalues.Precio;
import co.com.sofka.delivery.tienda.values.ServicioId;
import co.com.sofka.domain.generic.DomainEvent;

public class ServicioAgregado extends DomainEvent {
    private final ServicioId servicioId;
    private final Nombre nombre;
    private final Precio precio;

    public ServicioAgregado(ServicioId servicioId, Nombre nombre, Precio precio) {
        super("co.com.sofka.delivery.tienda.events.ServicioAgregado");
        this.servicioId = servicioId;
        this.nombre = nombre;
        this.precio = precio;
    }

    public ServicioId getServicioId() {
        return servicioId;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public Precio getPrecio() {
        return precio;
    }
}
