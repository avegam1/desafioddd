package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.genericvalues.Telefono;
import co.com.sofka.delivery.orden.values.OrdenId;
import co.com.sofka.delivery.orden.values.Propina;
import co.com.sofka.domain.generic.DomainEvent;

public class OrdenPreparada extends DomainEvent {
    private final OrdenId ordenId;
    private final Nombre nombre;
    private final Telefono telefono;
    private final Propina propina;

    public OrdenPreparada(OrdenId ordenId, Nombre nombre, Telefono telefono, Propina propina){
        super("co.com.sofka.delivery.orden.events.OrdenPreparada");
        this.ordenId = ordenId;
        this.nombre = nombre;
        this.telefono = telefono;
        this.propina = propina;
    }

    public OrdenId getOrdenId() {
        return ordenId;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public Propina getPropina() {
        return propina;
    }
}
