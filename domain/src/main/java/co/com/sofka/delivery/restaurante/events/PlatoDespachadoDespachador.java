package co.com.sofka.delivery.restaurante.events;

import co.com.sofka.delivery.restaurante.DespachadorId;
import co.com.sofka.delivery.restaurante.values.Plato;
import co.com.sofka.domain.generic.DomainEvent;

public class PlatoDespachadoDespachador extends DomainEvent {
    private final DespachadorId despachadorId;
    private final Plato plato;

    public PlatoDespachadoDespachador(DespachadorId despachadorId, Plato plato) {
        super("co.com.sofka.delivery.restaurante.events.PlatoDespachadoDespachador");
        this.despachadorId = despachadorId;
        this.plato = plato;
    }

    public DespachadorId getDespachadorId() {
        return despachadorId;
    }

    public Plato getPlato() {
        return plato;
    }
}
