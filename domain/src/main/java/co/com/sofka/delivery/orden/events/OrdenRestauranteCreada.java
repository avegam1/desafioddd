package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.cuenta.values.CuentaId;
import co.com.sofka.delivery.restaurante.values.RestauranteId;
import co.com.sofka.domain.generic.DomainEvent;

public class OrdenRestauranteCreada extends DomainEvent {
    private final RestauranteId restauranteId;
    private final CuentaId cuentaId;

    public OrdenRestauranteCreada(RestauranteId restauranteId, CuentaId cuentaId) {
        super("co.com.sofka.delivery.orden.events.OrdenRestauranteCreada");
        this.restauranteId = restauranteId;
        this.cuentaId = cuentaId;
    }

    public RestauranteId getRestauranteId() {
        return restauranteId;
    }

    public CuentaId getCuentaId() {
        return cuentaId;
    }
}
