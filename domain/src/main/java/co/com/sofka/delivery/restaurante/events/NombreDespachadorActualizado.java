package co.com.sofka.delivery.restaurante.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.restaurante.DespachadorId;
import co.com.sofka.domain.generic.DomainEvent;

public class NombreDespachadorActualizado  extends DomainEvent {
    private final DespachadorId despachadorId;
    private final Nombre nombre;

    public NombreDespachadorActualizado(DespachadorId despachadorId, Nombre nombre) {
        super("co.com.sofka.delivery.restaurante.events.NombreDespachadorActualizado");
        this.despachadorId = despachadorId;
        this.nombre = nombre;
    }

    public DespachadorId getDespachadorId() {
        return despachadorId;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
