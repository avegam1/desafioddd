package co.com.sofka.delivery.restaurante.events;

import co.com.sofka.delivery.genericvalues.CostoEnvio;
import co.com.sofka.domain.generic.DomainEvent;

public class CostoEnvioActualizado extends DomainEvent {
    private final CostoEnvio costoEnvio;

    public CostoEnvioActualizado(CostoEnvio costoEnvio) {
        super("co.com.sofka.delivery.restaurante.events.CostoEnvioActualizado");
        this.costoEnvio = costoEnvio;
    }

    public CostoEnvio getCostoEnvio() {
        return costoEnvio;
    }
}
