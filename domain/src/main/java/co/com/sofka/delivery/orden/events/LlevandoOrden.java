package co.com.sofka.delivery.orden.events;

import co.com.sofka.domain.generic.DomainEvent;

public class LlevandoOrden extends DomainEvent {
    public LlevandoOrden(){
        super("co.com.sofka.delivery.orden.events.LlevandoOrden");
    }
}
