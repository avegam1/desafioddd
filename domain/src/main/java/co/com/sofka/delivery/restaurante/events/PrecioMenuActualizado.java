package co.com.sofka.delivery.restaurante.events;

import co.com.sofka.delivery.genericvalues.Precio;
import co.com.sofka.delivery.restaurante.values.MenuId;
import co.com.sofka.domain.generic.DomainEvent;

public class PrecioMenuActualizado extends DomainEvent {
    private final MenuId menuId;
    private final Precio precio;

    public PrecioMenuActualizado(MenuId menuId, Precio precio) {
        super("co.com.sofka.delivery.restaurante.events.PrecioMenuActualizado");
        this.menuId = menuId;
        this.precio = precio;
    }

    public MenuId getMenuId() {
        return menuId;
    }

    public Precio getPrecio() {
        return precio;
    }
}
