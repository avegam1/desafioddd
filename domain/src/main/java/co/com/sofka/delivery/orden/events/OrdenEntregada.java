package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.genericvalues.Calificacion;
import co.com.sofka.delivery.orden.values.OrdenId;
import co.com.sofka.domain.generic.DomainEvent;

public class OrdenEntregada extends DomainEvent {
    private final OrdenId ordenId;
    private final Calificacion calificacion;

    public OrdenEntregada(OrdenId ordenId, Calificacion calificacion){
        super("co.com.sofka.delivery.orden.events.OrdenEntregada");
        this.ordenId = ordenId;
        this.calificacion = calificacion;
    }

    public OrdenId getOrdenId() {
        return ordenId;
    }

    public Calificacion getCalificacion() {
        return calificacion;
    }
}
