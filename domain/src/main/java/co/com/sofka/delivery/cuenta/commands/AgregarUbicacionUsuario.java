package co.com.sofka.delivery.cuenta.commands;

import co.com.sofka.delivery.cuenta.values.CuentaId;
import co.com.sofka.delivery.genericvalues.Ubicacion;
import co.com.sofka.domain.generic.Command;

public class AgregarUbicacionUsuario extends Command {
    private final CuentaId cuentaId;
    private final Ubicacion ubicacion;

    public AgregarUbicacionUsuario(CuentaId cuentaId, Ubicacion ubicacion){
        this.cuentaId = cuentaId;
        this.ubicacion = ubicacion;
    }

    public CuentaId getCuentaId() {
        return cuentaId;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }
}
