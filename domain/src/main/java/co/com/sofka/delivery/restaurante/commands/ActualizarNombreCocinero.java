package co.com.sofka.delivery.restaurante.commands;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.restaurante.values.CocineroId;
import co.com.sofka.delivery.restaurante.values.RestauranteId;
import co.com.sofka.domain.generic.Command;

public class ActualizarNombreCocinero extends Command {
    private final RestauranteId restauranteId;
    private final CocineroId cocineroId;
    private final Nombre nombre;

    public ActualizarNombreCocinero(RestauranteId restauranteId, CocineroId cocineroId, Nombre nombre){
        this.restauranteId = restauranteId;
        this.cocineroId = cocineroId;
        this.nombre = nombre;
    }

    public RestauranteId getRestauranteId() {
        return restauranteId;
    }

    public CocineroId getCocineroId() {
        return cocineroId;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
