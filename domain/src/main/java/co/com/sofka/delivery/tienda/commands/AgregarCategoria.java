package co.com.sofka.delivery.tienda.commands;

import co.com.sofka.delivery.genericvalues.Categoria;
import co.com.sofka.delivery.tienda.values.TiendaId;
import co.com.sofka.domain.generic.Command;

public class AgregarCategoria extends Command {
    private final TiendaId tiendaId;
    private final Categoria categoria;

    public AgregarCategoria(TiendaId tiendaId, Categoria categoria){
        this.tiendaId = tiendaId;
        this.categoria = categoria;
    }

    public TiendaId getTiendaId() {
        return tiendaId;
    }

    public Categoria getCategoria() {
        return categoria;
    }
}
