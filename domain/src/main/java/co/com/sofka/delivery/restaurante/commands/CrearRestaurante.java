package co.com.sofka.delivery.restaurante.commands;

import co.com.sofka.delivery.genericvalues.CostoEnvio;
import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.restaurante.values.RestauranteId;
import co.com.sofka.domain.generic.Command;

public class CrearRestaurante extends Command {
    private final RestauranteId restauranteId;
    private final Nombre nombre;
    private final CostoEnvio costoEnvio;

    public CrearRestaurante(RestauranteId restauranteId, Nombre nombre, CostoEnvio costoEnvio){
        this.restauranteId = restauranteId;
        this.nombre = nombre;
        this.costoEnvio = costoEnvio;
    }

    public RestauranteId getRestauranteId() {
        return restauranteId;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public CostoEnvio getCostoEnvio() {
        return costoEnvio;
    }
}
