package co.com.sofka.delivery.restaurante.events;

import co.com.sofka.delivery.restaurante.values.MenuId;
import co.com.sofka.delivery.restaurante.values.Plato;
import co.com.sofka.domain.generic.DomainEvent;

public class PlatoAgregadoMenu extends DomainEvent {
    private final MenuId menuId;
    private final Plato plato;

    public PlatoAgregadoMenu(MenuId menuId, Plato plato) {
        super("co.com.sofka.delivery.restaurante.events.PlatoAgregadoMenu");
        this.menuId = menuId;
        this.plato = plato;
    }

    public MenuId getMenuId() {
        return menuId;
    }

    public Plato getPlato() {
        return plato;
    }
}
