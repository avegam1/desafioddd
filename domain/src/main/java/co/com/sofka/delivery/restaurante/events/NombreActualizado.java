package co.com.sofka.delivery.restaurante.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.domain.generic.DomainEvent;

public class NombreActualizado  extends DomainEvent {private final Nombre nombre;

    public NombreActualizado(Nombre nombre) {
        super("co.com.sofka.delivery.restaurante.events.NombreActualizado");
        this.nombre = nombre;
    }

    public Nombre getNombre() {
        return nombre;
    }

}
