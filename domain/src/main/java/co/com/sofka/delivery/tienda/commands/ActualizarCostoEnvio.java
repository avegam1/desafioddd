package co.com.sofka.delivery.tienda.commands;

import co.com.sofka.delivery.genericvalues.CostoEnvio;
import co.com.sofka.delivery.tienda.values.TiendaId;
import co.com.sofka.domain.generic.Command;

public class ActualizarCostoEnvio extends Command {
    private final TiendaId tiendaId;
    private final CostoEnvio costoEnvio;

    public ActualizarCostoEnvio(TiendaId tiendaId, CostoEnvio costoEnvio){
        this.tiendaId = tiendaId;
        this.costoEnvio = costoEnvio;
    }

    public TiendaId getTiendaId() {
        return tiendaId;
    }

    public CostoEnvio getCostoEnvio() {
        return costoEnvio;
    }
}
