package co.com.sofka.delivery.cuenta.events;

import co.com.sofka.delivery.genericvalues.Ubicacion;
import co.com.sofka.domain.generic.DomainEvent;

public class UbicacionUsuarioAgregada extends DomainEvent {
    private final Ubicacion ubicacion;

    public UbicacionUsuarioAgregada(Ubicacion ubicacion) {
        super("co.com.rappi.delivery.cuenta.events.NombreUsuarioActualizado");
        this.ubicacion = ubicacion;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }
}
