package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.genericvalues.Telefono;
import co.com.sofka.delivery.orden.values.Propina;
import co.com.sofka.delivery.orden.values.RappiTenderoId;
import co.com.sofka.domain.generic.DomainEvent;

public class RappiTenderoAsignado extends DomainEvent {
    private final RappiTenderoId rappiTenderoId;
    private final Nombre nombre;
    private final Telefono telefono;
    private final Propina propina;

    public RappiTenderoAsignado(RappiTenderoId rappiTenderoId, Nombre nombre, Telefono telefono, Propina propina) {
        super("co.com.sofka.delivery.orden.events.OrdenRecibida");
        this.rappiTenderoId = rappiTenderoId;
        this.nombre = nombre;
        this.telefono = telefono;
        this.propina = propina;
    }

    public RappiTenderoId getRappiTenderoId() {
        return rappiTenderoId;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public Propina getPropina() {
        return propina;
    }
}
