package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.genericvalues.Precio;
import co.com.sofka.delivery.tienda.values.ServicioId;
import co.com.sofka.domain.generic.DomainEvent;

public class PrecioServicioActualizado extends DomainEvent {
    private final ServicioId servicioId;
    private final Precio precio;

    public PrecioServicioActualizado(ServicioId servicioId, Precio precio) {
        super("co.com.sofka.delivery.tienda.events.PrecioProductoActualizado");
        this.servicioId = servicioId;
        this.precio = precio;
    }

    public ServicioId getServicioId() {
        return servicioId;
    }

    public Precio getPrecio() {
        return precio;
    }
}
