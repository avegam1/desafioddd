package co.com.sofka.delivery.cuenta.events;

import co.com.sofka.domain.generic.DomainEvent;

public class RappiPrimeActualizadoAPlanBasico extends DomainEvent {
    public RappiPrimeActualizadoAPlanBasico() {
        super("co.com.sofka.delivery.cuenta.events.RappiPrimeActualizadoAPlanBasico");
    }
}
