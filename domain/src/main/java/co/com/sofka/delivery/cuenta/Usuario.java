package co.com.sofka.delivery.cuenta;

import co.com.sofka.delivery.cuenta.values.UsuarioId;
import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.genericvalues.Telefono;
import co.com.sofka.delivery.genericvalues.Ubicacion;
import co.com.sofka.domain.generic.Entity;

import java.util.HashSet;
import java.util.Set;

public class Usuario extends Entity<UsuarioId> {
    protected Nombre nombre;
    protected Set<Ubicacion> ubicaciones = new HashSet<>();
    protected Telefono telefono;

    public Usuario(UsuarioId usuarioId, Nombre nombre, Telefono telefono) {
        super(usuarioId);
        this.nombre = nombre;
        this.telefono = telefono;
    }

    public void actualizarNombre(String nombre){
        this.nombre = new Nombre(nombre);
    }

    public void actualizarTelefono(String telefono){
        this.telefono = new Telefono(telefono);
    }

    public void agregarUbicacion(Ubicacion ubicacion){
        ubicaciones.add(ubicacion);
    }
}
