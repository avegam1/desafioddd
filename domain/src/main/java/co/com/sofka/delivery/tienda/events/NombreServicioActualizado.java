package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.tienda.values.ServicioId;
import co.com.sofka.domain.generic.DomainEvent;

public class NombreServicioActualizado extends DomainEvent {
    private final ServicioId servicioId;
    private final Nombre nombre;

    public NombreServicioActualizado(ServicioId servicioId, Nombre nombre) {
        super("co.com.sofka.delivery.tienda.events.NombreServicioActualizado");
        this.servicioId = servicioId;
        this.nombre = nombre;
    }

    public ServicioId getServicioId() {
        return servicioId;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
