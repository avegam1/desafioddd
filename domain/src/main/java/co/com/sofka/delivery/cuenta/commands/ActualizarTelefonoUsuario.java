package co.com.sofka.delivery.cuenta.commands;

import co.com.sofka.delivery.cuenta.values.CuentaId;
import co.com.sofka.delivery.genericvalues.Telefono;
import co.com.sofka.domain.generic.Command;

public class ActualizarTelefonoUsuario extends Command {
    private final CuentaId cuentaId;
    private final Telefono telefono;

    public ActualizarTelefonoUsuario(CuentaId cuentaId, Telefono telefono){
        this.cuentaId = cuentaId;
        this.telefono = telefono;
    }

    public CuentaId getCuentaId() {
        return cuentaId;
    }

    public Telefono getTelefono() {
        return telefono;
    }
}
