package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.genericvalues.MedioPago;
import co.com.sofka.domain.generic.DomainEvent;

public class MedioPagoFacturaActualizado extends DomainEvent {
    private final MedioPago medioPago;

    public MedioPagoFacturaActualizado(MedioPago medioPago) {
        super("co.com.sofka.delivery.orden.events.MedioPagoFacturaActualizado");
        this.medioPago = medioPago;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }
}
