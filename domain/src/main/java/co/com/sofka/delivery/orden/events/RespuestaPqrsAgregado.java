package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.orden.values.Respuesta;
import co.com.sofka.domain.generic.DomainEvent;

public class RespuestaPqrsAgregado extends DomainEvent {
    private final Respuesta respuesta;

    public RespuestaPqrsAgregado(Respuesta respuesta) {
        super("co.com.sofka.delivery.orden.events.RespuestaPqrsAgregado");
        this.respuesta = respuesta;
    }

    public Respuesta getRespuesta() {
        return respuesta;
    }
}
