package co.com.sofka.delivery.tienda.commands;

import co.com.sofka.delivery.genericvalues.Calificacion;
import co.com.sofka.delivery.tienda.values.TiendaId;
import co.com.sofka.domain.generic.Command;

public class ActualizarCalificacion extends Command {
    private final TiendaId tiendaId;
    private final Calificacion calificacion;

    public ActualizarCalificacion(TiendaId tiendaId, Calificacion calificacion){
        this.tiendaId = tiendaId;
        this.calificacion = calificacion;
    }

    public TiendaId getTiendaId() {
        return tiendaId;
    }

    public Calificacion getCalificacion() {
        return calificacion;
    }
}
