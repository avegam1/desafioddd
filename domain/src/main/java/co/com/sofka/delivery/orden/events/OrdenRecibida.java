package co.com.sofka.delivery.orden.events;

import co.com.sofka.domain.generic.DomainEvent;

public class OrdenRecibida extends DomainEvent {
    public OrdenRecibida(){
        super("co.com.sofka.delivery.orden.events.OrdenRecibida");
    }
}
