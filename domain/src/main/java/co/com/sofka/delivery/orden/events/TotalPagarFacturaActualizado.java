package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.orden.values.TotalPagar;
import co.com.sofka.domain.generic.DomainEvent;

public class TotalPagarFacturaActualizado extends DomainEvent {
    private final TotalPagar totalPagar;

    public TotalPagarFacturaActualizado(TotalPagar totalPagar) {
        super("co.com.sofka.delivery.orden.events.TotalPagarFacturaActualizado");
        this.totalPagar = totalPagar;
    }

    public TotalPagar getTotalPagar() {
        return totalPagar;
    }
}
