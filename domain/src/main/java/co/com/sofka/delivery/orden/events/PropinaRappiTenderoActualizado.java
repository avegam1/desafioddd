package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.orden.values.Propina;
import co.com.sofka.domain.generic.DomainEvent;

public class PropinaRappiTenderoActualizado extends DomainEvent {
    private final Propina propina;

    public PropinaRappiTenderoActualizado(Propina propina) {
        super("co.com.sofka.delivery.orden.events.PropinaRappiTenderoActualizado");
        this.propina = propina;
    }

    public Propina getPropina() {
        return propina;
    }
}
