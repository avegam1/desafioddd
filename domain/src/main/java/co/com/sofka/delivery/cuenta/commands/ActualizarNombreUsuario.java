package co.com.sofka.delivery.cuenta.commands;

import co.com.sofka.delivery.cuenta.values.CuentaId;
import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.domain.generic.Command;

public class ActualizarNombreUsuario extends Command {
    private final CuentaId cuentaId;
    private final Nombre nombre;

    public ActualizarNombreUsuario(CuentaId cuentaId, Nombre nombre){
        this.cuentaId = cuentaId;
        this.nombre = nombre;
    }

    public CuentaId getCuentaId() {
        return cuentaId;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
