package co.com.sofka.delivery.cuenta.commands;

import co.com.sofka.delivery.cuenta.Usuario;
import co.com.sofka.delivery.cuenta.values.CuentaId;
import co.com.sofka.domain.generic.Command;

public class CrearCuenta extends Command {
    private final CuentaId cuentaId;
    private final Usuario usuario;

    public CrearCuenta(CuentaId cuentaId, Usuario usuario){
        this.cuentaId = cuentaId;
        this.usuario = usuario;
    }

    public CuentaId getCuentaId() {
        return cuentaId;
    }

    public Usuario getUsuario() {
        return usuario;
    }
}
