package co.com.sofka.delivery.orden;

import co.com.sofka.delivery.genericvalues.Calificacion;
import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.genericvalues.Telefono;
import co.com.sofka.delivery.orden.values.Propina;
import co.com.sofka.delivery.orden.values.RappiTenderoId;
import co.com.sofka.domain.generic.Entity;

public class RappiTendero extends Entity<RappiTenderoId> {
    protected Nombre nombre;
    protected Calificacion calificacion;
    protected Telefono telefono;
    protected Propina propina;

    public RappiTendero(RappiTenderoId rappiTenderoId, Nombre nombre, Telefono telefono, Propina propina) {
        super(rappiTenderoId);
        this.nombre = nombre;
        this.telefono = telefono;
        this.propina = propina;
    }

    public void agregarCalificacion(Calificacion calificacion){
        this.calificacion = calificacion;
    }

    public void actualizarPropina(Double propina){
        this.propina = new Propina(propina);
    }
}
