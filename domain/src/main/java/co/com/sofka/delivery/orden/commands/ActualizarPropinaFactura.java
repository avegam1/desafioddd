package co.com.sofka.delivery.orden.commands;

import co.com.sofka.delivery.orden.values.OrdenId;
import co.com.sofka.delivery.orden.values.Propina;
import co.com.sofka.domain.generic.Command;

public class ActualizarPropinaFactura extends Command {
    private final OrdenId ordenId;
    private final Propina propina;

    public  ActualizarPropinaFactura(OrdenId ordenId, Propina propina){
        this.ordenId = ordenId;
        this.propina = propina;
    }

    public OrdenId getOrdenId() {
        return ordenId;
    }

    public Propina getPropina() {
        return propina;
    }
}
