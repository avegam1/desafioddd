package co.com.sofka.delivery.restaurante.events;

import co.com.sofka.delivery.restaurante.values.CocineroId;
import co.com.sofka.delivery.restaurante.values.Plato;
import co.com.sofka.domain.generic.DomainEvent;

public class PlatoCocinadoCocinero extends DomainEvent {
    private final CocineroId cocineroId;
    private final Plato plato;

    public PlatoCocinadoCocinero(CocineroId cocineroId, Plato plato) {
        super("co.com.sofka.delivery.restaurante.events.PlatoCocinadoCocinero");
        this.cocineroId = cocineroId;
        this.plato = plato;
    }

    public CocineroId getCocineroId() {
        return cocineroId;
    }

    public Plato getPlato() {
        return plato;
    }
}
