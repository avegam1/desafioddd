package co.com.sofka.delivery.cuenta.events;

import co.com.sofka.domain.generic.DomainEvent;

public class RappiPrimeActualizadoAPlanPlus  extends DomainEvent {
    public RappiPrimeActualizadoAPlanPlus() {
        super("co.com.sofka.delivery.cuenta.events.RappiPrimeActualizadoAPlanPlus");
    }
}
