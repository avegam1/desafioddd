package co.com.sofka.delivery.cuenta.events;

import co.com.sofka.delivery.cuenta.Usuario;
import co.com.sofka.domain.generic.DomainEvent;

public class CuentaCreada extends DomainEvent {
    private final Usuario usuario;

    public CuentaCreada(Usuario usuario) {
        super("co.com.sofka.delivery.cuenta.events.CuentaCreada");
        this.usuario = usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }
}
