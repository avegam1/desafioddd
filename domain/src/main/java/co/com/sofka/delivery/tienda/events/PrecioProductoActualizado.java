package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.genericvalues.Precio;
import co.com.sofka.delivery.tienda.values.ProductoId;
import co.com.sofka.domain.generic.DomainEvent;

public class PrecioProductoActualizado extends DomainEvent {
    private final ProductoId productoId;
    private final Precio precio;

    public PrecioProductoActualizado(ProductoId productoId, Precio precio) {
        super("co.com.sofka.delivery.tienda.events.PrecioProductoActualizado");
        this.productoId = productoId;
        this.precio = precio;
    }

    public ProductoId getProductoId() {
        return productoId;
    }

    public Precio getPrecio() {
        return precio;
    }
}
