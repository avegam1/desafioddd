package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.tienda.values.EmpleadoId;
import co.com.sofka.delivery.tienda.values.ServicioId;
import co.com.sofka.domain.generic.DomainEvent;

public class ServicioPrestadoEmpleado extends DomainEvent {
    private final EmpleadoId empleadoId;
    private final ServicioId servicioId;

    public ServicioPrestadoEmpleado(EmpleadoId empleadoId, ServicioId servicioId) {
        super("co.com.rappi.delivery.tienda.events.ServicioPrestadoEmpleado");
        this.empleadoId = empleadoId;
        this.servicioId = servicioId;
    }

    public EmpleadoId getEmpleadoId() {
        return empleadoId;
    }

    public ServicioId getServicioId() {
        return servicioId;
    }
}
