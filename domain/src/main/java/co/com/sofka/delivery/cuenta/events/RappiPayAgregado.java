package co.com.sofka.delivery.cuenta.events;

import co.com.sofka.delivery.cuenta.values.RappiPayId;
import co.com.sofka.delivery.cuenta.values.Saldo;
import co.com.sofka.domain.generic.DomainEvent;

public class RappiPayAgregado extends DomainEvent {
    private final RappiPayId rappiPayId;
    private final Saldo saldo;

    public RappiPayAgregado(RappiPayId rappiPayId, Saldo saldo) {
        super("co.com.sofka.delivery.cuenta.events.RappiPayAgregado");
        this.rappiPayId = rappiPayId;
        this.saldo = saldo;
    }

    public RappiPayId getRappiPayId() {
        return rappiPayId;
    }

    public Saldo getSaldo() {
        return saldo;
    }
}
