package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.genericvalues.Calificacion;
import co.com.sofka.domain.generic.DomainEvent;

public class CalificacionRappiTenderoAgregada extends DomainEvent {
    private final Calificacion calificacion;

    public CalificacionRappiTenderoAgregada(Calificacion calificacion) {
        super("co.com.sofka.delivery.orden.events.CalificacionRappiTenderoAgregada");
        this.calificacion = calificacion;
    }

    public Calificacion getCalificacion() {
        return calificacion;
    }
}
