package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.orden.values.Propina;
import co.com.sofka.domain.generic.DomainEvent;

public class PropinaFacturaActualizada extends DomainEvent {
    private final Propina propina;

    public PropinaFacturaActualizada(Propina propina) {
        super("co.com.sofka.delivery.orden.events.PropinaFacturaActualizada");
        this.propina = propina;
    }

    public Propina getPropina() {
        return propina;
    }
}
