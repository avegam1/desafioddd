package co.com.sofka.delivery.restaurante.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.restaurante.values.CocineroId;
import co.com.sofka.domain.generic.DomainEvent;

public class CocineroAgregado extends DomainEvent {
    private final CocineroId cocineroId;
    private final Nombre nombre;

    public CocineroAgregado(CocineroId cocineroId, Nombre nombre) {
        super("co.com.sofka.delivery.restaurante.events.CocineroAgregado");
        this.cocineroId = cocineroId;
        this.nombre = nombre;
    }

    public CocineroId getCocineroId() {
        return cocineroId;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
