package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.genericvalues.Categoria;
import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.genericvalues.Precio;
import co.com.sofka.delivery.tienda.values.ProductoId;
import co.com.sofka.domain.generic.DomainEvent;

public class ProductoAgregado extends DomainEvent {
    private final ProductoId productoId;
    private final Categoria categoria;
    private final Precio precio;
    private final Nombre nombre;

    public ProductoAgregado(ProductoId productoId, Categoria categoria, Precio precio, Nombre nombre) {
        super("co.com.sofka.delivery.tienda.events.ProductoAgregado");
        this.productoId = productoId;
        this.categoria = categoria;
        this.precio = precio;
        this.nombre = nombre;
    }

    public ProductoId getProductoId() {
        return productoId;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public Precio getPrecio() {
        return precio;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
