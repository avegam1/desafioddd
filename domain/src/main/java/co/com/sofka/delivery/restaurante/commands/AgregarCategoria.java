package co.com.sofka.delivery.restaurante.commands;

import co.com.sofka.delivery.genericvalues.Categoria;
import co.com.sofka.delivery.restaurante.values.RestauranteId;
import co.com.sofka.domain.generic.Command;

public class AgregarCategoria extends Command {
    private final RestauranteId restauranteId;
    private final Categoria categoria;

    public AgregarCategoria(RestauranteId restauranteId, Categoria categoria){
        this.restauranteId = restauranteId;
        this.categoria = categoria;
    }

    public RestauranteId getRestauranteId() {
        return restauranteId;
    }

    public Categoria getCategoria() {
        return categoria;
    }

}
