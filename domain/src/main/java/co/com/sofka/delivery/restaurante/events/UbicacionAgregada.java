package co.com.sofka.delivery.restaurante.events;

import co.com.sofka.delivery.genericvalues.Ubicacion;
import co.com.sofka.domain.generic.DomainEvent;

public class UbicacionAgregada extends DomainEvent {
    private final Ubicacion ubicacion;

    public UbicacionAgregada(Ubicacion ubicacion) {
        super("co.com.sofka.delivery.restaurante.events.UbicacionAgregada");
        this.ubicacion = ubicacion;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }
}
