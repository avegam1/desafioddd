package co.com.sofka.delivery.orden.events;

import co.com.sofka.delivery.genericvalues.MedioPago;
import co.com.sofka.delivery.orden.values.FacturaId;
import co.com.sofka.delivery.orden.values.Fecha;
import co.com.sofka.delivery.orden.values.Propina;
import co.com.sofka.domain.generic.DomainEvent;

public class FacturaGenerada extends DomainEvent {
    private final FacturaId facturaId;
    private final Fecha fecha;
    private final MedioPago medioPago;
    private final Propina propina;

    public FacturaGenerada(FacturaId facturaId, Fecha fecha, MedioPago medioPago, Propina propina) {
        super("co.com.sofka.delivery.orden.events.MedioPagoFacturaActualizado");
        this.facturaId = facturaId;
        this.fecha = fecha;
        this.medioPago = medioPago;
        this.propina = propina;
    }

    public FacturaId getFacturaId() {
        return facturaId;
    }

    public Fecha getFecha() {
        return fecha;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }

    public Propina getPropina() {
        return propina;
    }
}
