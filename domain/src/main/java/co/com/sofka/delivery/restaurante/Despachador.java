package co.com.sofka.delivery.restaurante;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.restaurante.values.Plato;
import co.com.sofka.domain.generic.Entity;

public class Despachador extends Entity<DespachadorId> {
    protected Nombre nombre;
    protected Plato plato;

    public Despachador(DespachadorId despachadorId, Nombre nombre) {
        super(despachadorId);
        this.nombre = nombre;
    }

    public void despacharPlato(Plato plato){
        this.plato = plato;
    }

    public void actualizarNombre(String nombre){
        this.nombre = new Nombre(nombre);
    }
}
