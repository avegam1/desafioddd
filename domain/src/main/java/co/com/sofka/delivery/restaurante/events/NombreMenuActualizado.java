package co.com.sofka.delivery.restaurante.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.restaurante.values.MenuId;
import co.com.sofka.domain.generic.DomainEvent;

public class NombreMenuActualizado extends DomainEvent {
    private final MenuId menuId;
    private final Nombre nombre;

    public NombreMenuActualizado(MenuId menuId, Nombre nombre) {
        super("co.com.sofka.delivery.restaurante.events.ActualizarNombreMenu");
        this.menuId = menuId;
        this.nombre = nombre;
    }

    public MenuId getMenuId() {
        return menuId;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
