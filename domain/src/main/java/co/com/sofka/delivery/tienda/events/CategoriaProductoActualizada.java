package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.genericvalues.Categoria;
import co.com.sofka.delivery.tienda.values.ProductoId;
import co.com.sofka.domain.generic.DomainEvent;

public class CategoriaProductoActualizada extends DomainEvent {
    private final ProductoId productoId;
    private final Categoria categoria;

    public CategoriaProductoActualizada(ProductoId productoId, Categoria categoria) {
        super("co.com.sofka.delivery.tienda.events.CategoriaAgregada");
        this.productoId = productoId;
        this.categoria = categoria;
    }

    public ProductoId getProductoId() {
        return productoId;
    }

    public Categoria getCategoria() {
        return categoria;
    }
}
