package co.com.sofka.delivery.tienda.events;

import co.com.sofka.delivery.genericvalues.Nombre;
import co.com.sofka.delivery.tienda.values.ProductoId;
import co.com.sofka.domain.generic.DomainEvent;

public class NombreProductoActualizado extends DomainEvent {
    private final ProductoId productoId;
    private final Nombre nombre;

    public NombreProductoActualizado(ProductoId productoId, Nombre nombre) {
        super("co.com.sofka.delivery.tienda.events.NombreProductoActualizado");
        this.productoId = productoId;
        this.nombre = nombre;
    }

    public ProductoId getProductoId() {
        return productoId;
    }

    public Nombre getNombre() {
        return nombre;
    }
}
