package co.com.sofka.delivery.cuenta.events;

import co.com.sofka.delivery.genericvalues.MedioPago;
import co.com.sofka.domain.generic.DomainEvent;

public class MedioPagoRappiPrimeActualizado extends DomainEvent {
    private final MedioPago medioPago;

    public MedioPagoRappiPrimeActualizado(MedioPago medioPago) {
        super("co.com.sofka.delivery.cuenta.events.MedioPagoRappiPrimeActualizado");
        this.medioPago = medioPago;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }
}
